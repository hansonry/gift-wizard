class CreateGifts < ActiveRecord::Migration[6.1]
  def change
    create_table :gifts do |t|
      t.string     :name
      t.text       :description
      t.belongs_to :creator_user, foreign_key: {to_table: :users}
      t.belongs_to :for_user,     foreign_key: {to_table: :users}
      t.belongs_to :group

      t.timestamps
    end
  end
end
