# Gift orgainizer


## Use case example

* Lists exchanges are limmited to a group of people
* Kat can create a list for gift ideas for herself. This list gets shared with everyone.
* Steff can think of things to add (like an orange). Kat doesn't see this, but everyone else does.
* Kat can add stuff to her list, everyone else can see the new stuff she adds.
* Somone else in the group can claim the particluar gift. But Kat can't see who or that it has been claimed.
* Anyone other than Kat can claim, even someone who didn't "think up" the gift idea.
* Notes can be added to the gifts, but Kat can't see them.
* Hyperlink support.
* Not tied to a specific holiday or event



