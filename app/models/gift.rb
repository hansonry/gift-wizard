class Gift < ApplicationRecord
  belongs_to :creator_user, :class_name => 'User', :foreign_key => 'creator_user_id'
  belongs_to :for_user,     :class_name => 'User', :foreign_key => 'for_user_id'
  belongs_to :group
end
