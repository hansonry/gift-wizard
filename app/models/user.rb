class User < ApplicationRecord
  has_secure_password

  validates :email, presence: true, uniqueness: true, format: { with: /\A[^@\s]+@[^@\s]+\z/, message: 'Invalid email' }
  has_and_belongs_to_many :groups
  has_many :created_gifts, :class_name => 'Gift', :foreign_key => 'creator_user_id'
  has_many :for_gifts,     :class_name => 'Gift', :foreign_key => 'for_user_id'
end
