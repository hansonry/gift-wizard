class GroupsController < ApplicationController
  before_action :require_user_logged_in!
  def new
    @group = Group.new
  end
  def create
    @group = Group.new(group_params)
    @group.users << Current.user

    if @group.save
      redirect_to @group
    else
      render :new
    end
  end
 
  def show
    @group = Group.find(params[:id])
    if not @group.users.exists?(Current.user.id)
       flash[:alert] = 'You are not a member of group: ' + @group.name
       redirect_to groups_path
    end
  end

  def index
    @groups = Current.user.groups
  end
  def all
    # Secret all group page
    @groups = Group.all
  end

  def adduser
    @user_params = user_email_params
    @user  = User.find_by_email(@user_params[:email])
    @group = Group.find(params[:id])
    if @user.nil?
      flash[:alert] = 'Invalid email address: ' + @user_params[:email]
    elsif @group.users.exists?(@user.id)
      flash[:alert] = 'Email address already in group: ' + @user_params[:email]
    else
      @group.users << @user
      @group.save
    end
    redirect_to @group
  end
 
  private
  def group_params
    params.require(:group).permit(:name, :description)
  end
  def user_email_params
    params.require(:user).permit(:email)
  end
end
